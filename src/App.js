import React from 'react';
import Game from './Game';


/** Simple app that just shows the LightsOut game. */

const App = () => {
  return (
    <div className="App">
      <Game/>
    </div>
);
}

export default App;