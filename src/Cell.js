import React from 'react';
import './Cell.css';

// This is the "light" square
const Cell = (props) => {
  const handleClick = () => {
    props.flipCellsAroundMe(); // function to be call when click on the Cell component
  }

  
  let classes = 'Cell' + (props.isLit ? ' Cell-lit' : '') //change color of the cell (ON/OFF)
  return (
  <div className={classes} onClick={handleClick} />
  );
}

export default Cell;

