import React, { Component , useState, useEffect } from 'react';
import Cell from './Cell';
import './Board.css';

/** Game board of Lights out.
 *
 * Properties:
 *
 * - nrows: number of rows of board
 * - ncols: number of cols of board
 * - Difficulty: int, number of permutations to mess up the board at start of game
 *
 * State:
 *
 * - hasWon: boolean, true when board is all off
 * - board: array-of-arrays of true/false
 *
 *    For this board:
 *       .  .  .
 *       O  O  .     (where . is off, and O is on)
 *       .  .  .
 *
 *    This would be: [[f, f, f], [t, t, f], [f, f, f]]
 *
 *  This should render a grid of individual <Cell /> components.
 *
 *  This doesn't handle any clicks --- clicks are on individual cells
 *
 **/


function Board (props) {

  const defaultProps = {
    nrows: 5,
    ncols: 5,
    // chanceLightStartsOn: 0.1,
    difficulty: 5
  };





  //returns an array-of-arrays of true/false values representing the state of the board (lighted and unlighted cells)
  const createBoard = () => {
    let board = [];
    let { ncols, nrows } = props;

    for (let j = 0; j < nrows; j++) {
      //make each row
      let row = [];
      for (let i = 0; i < ncols; i++) {
        row.push(false);

      }
      board.push(row);
    }

    return board;
  }


  //determines which lights are "on" to start the game (ensures a solveable puzzle)
  const setUpPuzzle = () => {
    let { ncols, nrows } = props;
    //randomly generate list of cells to "click" to set up the puzzle
    let cellsToClick = [];
    for (let j = 0; j < props.difficulty; j++) {
      let x = Math.floor(Math.random() * ncols);
      let y = Math.floor(Math.random() * nrows);
      let coord = `${y}-${x}`;
      cellsToClick.push(coord);
    }

    // "click" cells by calling flipCellsAround on random coord
    cellsToClick.forEach(coord => flipCellsAround(coord));
  }

  /** handle changing a cell: update board & determine if winner */
  const flipCellsAround = (coord) => {
    let { ncols, nrows } = props;
    // let board = state.board;
    let [y, x] = coord.split('-').map(Number);

    function flipCell(y, x) {
      // if this coord is actually on board, flip it
      if (x >= 0 && x < ncols && y >= 0 && y < nrows) {
        board[y][x] = !board[y][x];
      }
    }


    // When cell is clicked, flip the cell and the cells around it
    flipCell(y, x);
    flipCell(y, x + 1);
    flipCell(y, x - 1);
    flipCell(y - 1, x);
    flipCell(y + 1, x);

    // Determine if the game has been won (win when every cell is turned off)
    hasWon = true;
    for (let i = 0; i < board.length; i++) {
      for (let j = 0; j < board[i].length; j++) {
        if (board[i][j]) {
          hasWon = false;
        }
      }
    }
    setBoard(board);
    setHasWon(hasWon);


  }
  
  const reStartGame = () => {
    // let board = createBoard();
    let hasWon = false;
    // this.setState({ board, hasWon }, () => this.setUpPuzzle());
    // this.setState({ board, hasWon }, () => this.setUpPuzzle());
    // this.setState({ board, hasWon }, () => this.setUpPuzzle());
    setBoard(setUpPuzzle());
    setHasWon(hasWon);

  
  }
      //state
      const [hasWon, setHasWon] = useState(false);
      const [board, setBoard] = useState(createBoard());
      
    
    //TODO: add sliding scale to change difficulty of game(will change prop for "difficulty")
    //needs to be passed from parent
    useEffect(() => {
      setUpPuzzle();
    });
  

  /** Render game board or winning message. */

  
    // if the game is won, just show a winning msg & render nothing else
    let winningMessage = (
      <div>
        <h2>You win!</h2>
        <button onClick={() => reStartGame()}>Play again</button>
      </div>
    );

    // make table board
    let tableBoard = [];
    // let { board } = state;
    for (let i = 0; i < board.length; i++) {
      let row = [];
      for (let j = 0; j < board[i].length; j++) {
        let coord = `${i}-${j}`;
        row.push(
          <Cell
            isLit={board[i][j]}
            key={coord}
            flipCellsAroundMe={flipCellsAround(coord)}
          />
        );
      }
      tableBoard.push(
        <div key={i} className="row">
          {row}
        </div>
      );
    }



  return (
    <>
        <h1>Lights Out</h1>
        {hasWon ? (
            winningMessage
        ) : (
            <div id="board">{tableBoard}</div>
        )}
    </>
  );
}

export default Board;


