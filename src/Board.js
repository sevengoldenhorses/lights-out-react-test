import React, { Component } from 'react';
import Cell from './Cell';
import './Board.css';



class Board extends Component {
  static defaultProps = {
    nrows: 5,
    ncols: 5, 
    difficulty: 5 // Level of starting lights ON
  };

  constructor(props) {
    super(props);
    this.state = {
      hasWon: false,
      board: this.createBoard()
    };
  }

  //TODO: add sliding scale to change difficulty of game(will change prop for "difficulty")
  //needs to be passed from parent

  componentDidMount() {
    this.setUpPuzzle();
  }

  //returns an two-dimensional array of true/false values representing the state of the board (light-on/light-off cells)
  createBoard() {
    let board = [];
    let { ncols, nrows } = this.props;

    for (let j = 0; j < nrows; j++) {
      //generate each row
      let row = [];
      for (let i = 0; i < ncols; i++) {
        row.push(false);

      }
      board.push(row);
    }

    return board;
  }

  //generate starting lights ON
  setUpPuzzle() {
    let { ncols, nrows } = this.props;
    //randomly generate list of cells to click to set up the puzzle
    let cellsToClick = [];
    for (let j = 0; j < this.props.difficulty; j++) {
      let x = Math.floor(Math.random() * ncols);
      let y = Math.floor(Math.random() * nrows);
      let coord = `${y}-${x}`;
      cellsToClick.push(coord);
    }

    // change the state of the cells by calling flipCellsAround on random coordinate
    cellsToClick.forEach(coord => this.flipCellsAround(coord));
  }

  // handle turn on/off light + update board + check if you win 
  flipCellsAround(coord) {
    let { ncols, nrows } = this.props;
    let board = this.state.board;
    let [y, x] = coord.split('-').map(Number);

    function flipCell(y, x) {
      // if coordinate is on the board than change its boolean value
      if (x >= 0 && x < ncols && y >= 0 && y < nrows) {
        board[y][x] = !board[y][x];
      }
    }

    // When cell is clicked, flip the cell and the cells around it
    flipCell(y, x); // The ones clicked
    flipCell(y, x + 1); // Right
    flipCell(y, x - 1); // Left
    flipCell(y - 1, x); // Down
    flipCell(y + 1, x); // Up
  
  // YOU WIN evaluation (it must find zero light ON = all rows must have false value inside)
  let hasWon = true;
  for (let y = 0; y < this.props.nrows; y++) {     
    for (let x = 0; x < this.props.ncols; x++) {
      if(board[y][x] === true){ // true = light ON
        hasWon = false;
        break;
      }
    }          
  }
  this.setState({ board, hasWon });
}

  reStartGame() {
    let board = this.createBoard();
    let hasWon = false;
    this.setState({ board, hasWon }, () => this.setUpPuzzle());
  }

  // Render game board or winning message.

  render() {
    
    let winningMessage = (
      <div>
        <h2>YOU WIN!</h2>
        <button onClick={() => this.reStartGame()}>PLAY AGAIN?</button>
      </div>
    );

    // Make table board
    let tableBoard = [];
    let { board } = this.state;
    for (let i = 0; i < board.length; i++) {
      let row = [];
      for (let j = 0; j < board[i].length; j++) {
        let coord = `${i}-${j}`;
        row.push(
          <Cell
            isLit={board[i][j]}
            key={coord}
            flipCellsAroundMe={this.flipCellsAround.bind(this, coord)}
          />
        );
      }
      tableBoard.push(
        <div key={i} className="row">
          {row}
        </div>
      );
    }

    return (
      <div>
        <h1>LIGHTS OUT</h1>
        {this.state.hasWon ? (
          winningMessage
        ) : (
          <div id="board">{tableBoard}</div>
        )}
      </div>
    );
  }
}

export default Board;
